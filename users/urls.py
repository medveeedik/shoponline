from django.conf.urls import url
from users.api import BalanceAPI


urlpatterns = [
    url(r'^balance/$', BalanceAPI.as_view()),
]