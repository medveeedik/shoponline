from decimal import Decimal

from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.dispatch import receiver
from django.db.models import signals, Sum


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        email = self.normalize_email(email)
        user = self.model(email=email, is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), unique=True)
    username = models.CharField(_('Username'), unique=True, db_index=True,
                                max_length=256, blank=False)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text='Designates whether the user can log '
                                             'into this admin site.')
    is_active = models.BooleanField('active', default=True,
                                    help_text='Designates whether this user should be treated as '
                                              'active. Unselect this instead of deleting accounts.')
    date_joined = models.DateTimeField(_('date/time joined'), default=timezone.now,)

    USERNAME_FIELD = 'email'
    objects = UserManager()

    @property
    def actual_deposits(self):
        from basket.models import Transfers
        return self.transfers.filter(payment_status=Transfers.APPROVED,
                                     payment_type=Transfers.DEPOSIT)\
                             .aggregate(Sum('amount'))['amount__sum'] or Decimal()

    @property
    def actual_withdraw(self):
        from basket.models import Transfers
        return self.transfers.filter(payment_status=Transfers.APPROVED,
                                     payment_type=Transfers.WITHDRAW)\
                             .aggregate(Sum('amount'))['amount__sum'] or Decimal()

    @property
    def pending_deposits(self):
        from basket.models import Transfers
        return self.transfers.filter(payment_status=Transfers.PENDING,
                                     payment_type=Transfers.DEPOSIT)\
                              .aggregate(Sum('amount'))['amount__sum'] or Decimal()

    @property
    def pending_withdraw(self):
        from basket.models import Transfers
        return self.transfers.filter(payment_status=Transfers.PENDING,
                                     payment_type=Transfers.WITHDRAW)\
                             .aggregate(Sum('amount'))['amount__sum'] or Decimal()

    @property
    def actual_balance(self):
        return self.actual_deposits - self.actual_withdraw

    @property
    def balance_values(self):
        return {
            'actual': self.actual_balance,
            'pending_deposits': self.pending_deposits,
            'pending_withdraws': self.pending_withdraw,
            'actual_deposits': self.actual_deposits,
            'actual_withdraws': self.actual_withdraw
        }

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        ordering = ('-date_joined', )

    def __str__(self):
        return self.email


@receiver(signals.post_save, sender=User)
def create_basket_on_user_creation(sender, instance=None, created=False, **kwargs):
    from basket.models import Basket
    if created:
        Basket.objects.create(user=instance)
