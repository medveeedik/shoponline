from setuptools import setup

setup(
    name='shoponline',
    version='',
    packages=['base', 'base.migrations', 'good', 'good.migrations', 'users', 'users.migrations',
              'basket', 'basket.migrations', 'beershop'],
    url='',
    license='',
    author='veronika',
    author_email='',
    description=''
)
