from django.conf.urls import url

from rest_framework import routers


class CustomRouter(routers.SimpleRouter):

    def get_urls(self):
        ret = []

        for prefix, viewset, basename in self.registry:

            lookup = self.get_lookup_regex(viewset)
            routes = self.get_routes(viewset)

            for route in routes:

                mapping = self.get_method_map(viewset, route.mapping)

                if not mapping:
                    continue

                regex = route.url.format(
                    prefix=prefix,
                    lookup=lookup,
                    trailing_slash=self.trailing_slash)

                view = viewset.as_view(mapping, **route.initkwargs)
                name = route.name.format(basename=basename)

                view_url = url(regex, view, name=name)
                view_url.callback.cls = viewset
                view_url.callback.route = route
                view_url.callback.cls.mapping = mapping

                ret.append(view_url)

        return ret
