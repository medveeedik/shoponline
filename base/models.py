import uuid

from django.db import models
from django.utils.translation import ugettext_lazy as _


# Create your models here.
class UUIDModel(models.Model):
    """
    An abstract base class model that makes primary key `id` as UUID
    instead of default auto incremented number.
    """
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    created = models.DateTimeField(_("date/time created"), auto_now_add=True, editable=False,
                                   null=True)
    modified = models.DateTimeField(auto_now=True, null=True)

    @classmethod
    def subscribe_on_save(cls, condition):
        if not hasattr(cls, '_on_save_listeners'):
            setattr(cls, '_on_save_listeners', [])

        def wrapper(function):
            cls._on_save_listeners.append({
                'condition': condition,
                'function': function
            })

            return function

        return wrapper

    def save(self, *args, **kwargs):
        cls = type(self)

        if hasattr(self, '_on_save_listeners'):
            prev_state = cls.objects.filter(id=self.id).first()

        super().save(*args, **kwargs)

        if hasattr(cls, '_on_save_listeners') and prev_state:
            for listener in filter(
                lambda listener: listener['condition'](prev_state, self),
                cls._on_save_listeners
            ):
                listener['function'](prev_state, self)

    class Meta:
        abstract = True
