from rest_framework import mixins, viewsets, decorators
from rest_framework.permissions import IsAuthenticated
from basket.models import BasketGoods, Basket, Transfers, Purchase, PurchaseGoods
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes


from basket import serializers
from users.models import User
from decimal import Decimal
from django.db.models import Sum, F


class OrdersAPI(
    mixins.CreateModelMixin, mixins.ListModelMixin, mixins.UpdateModelMixin,
    mixins.RetrieveModelMixin, mixins.DestroyModelMixin, viewsets.GenericViewSet
):
    permission_classes = [IsAuthenticated]

    serializer_class = serializers.OrdersSerializer
    update_serializer_class = serializers.UpdateOrderSerializer

    def get_queryset(self):

        queryset = BasketGoods.objects\
            .filter(basket__user=self.request.user)

        return queryset

    def get_serializer_class(self):

        if self.action == 'update':
            return self.update_serializer_class

        return self.serializer_class

    def list(self, *args, **kwargs):

        """
        Get a list of users orders. Get a list of users orders. There will only unclosed orders.
        """
        return super().list(*args, **kwargs)

    def create(self, *args, **kwargs):

        """
        Create an order.
        """
        from basket.models import BasketGoods
        from good.models import Good

        item = BasketGoods.objects.create(
            good=Good.objects.get(id=str(args[0].data['good'])),
            amount=args[0].data['amount'],
            basket=self.request.user.basket)

        return Response(
            self.serializer_class(item).data,
            status=201)


class TransfersAPI(mixins.ListModelMixin, mixins.RetrieveModelMixin,
                   mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = [IsAuthenticated]

    serializer_class = serializers.TransfersSerializer

    def get_queryset(self):
        queryset = Transfers.objects\
            .filter(user=self.request.user)

        return queryset


class PurchasesAPI(
    mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet
):
    permission_classes = [IsAuthenticated]

    serializer_class = serializers.PurchaseSerializer

    def get_queryset(self):

        queryset = Purchase.objects\
            .filter(user=self.request.user)

        return queryset

    def list(self, *args, **kwargs):

        """
        Get a list of users orders. Get a list of users orders. There will only unclosed orders.
        """
        return super().list(*args, **kwargs)


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
def purchase_items_from_basket(request):
    user = request.user
    basket = request.user.basket
    goods = request.user.basket.basketgoods_set.all()
    if not goods.exists():
        return Response(
            {'errors': [{'message': 'Please add goods to basket first.'}]},
            status=400)
    total_cost = request.user.basket.basketgoods_set.all() \
        .aggregate(total_cost=Sum(F('amount')*F('good__cost')))['total_cost'] or Decimal()

    if user.actual_balance < total_cost:
        return Response(
            {'errors': [{'message': 'Not enough money.'}]},
            status=400)

    new_purchase = Purchase(user=user, transfer=Transfers.objects.create(
        amount=total_cost, payment_type=Transfers.WITHDRAW, user=user))

    new_purchase.save()

    for basketgood in basket.basketgoods_set.all():
        PurchaseGoods.objects.create(purchase=new_purchase, good=basketgood.good,
                                     amount=basketgood.amount)
    basket.clear()

    return Response(serializers.PurchaseSerializer(new_purchase).data, status=201)
