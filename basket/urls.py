from django.conf.urls import url, include
from basket.api import OrdersAPI, TransfersAPI, purchase_items_from_basket, PurchasesAPI

from base.routers import CustomRouter


router = CustomRouter(trailing_slash=True)
router.register(r'order', OrdersAPI, base_name='orders')
router.register(r'transfers', TransfersAPI, base_name='transfers')
router.register(r'purchases', PurchasesAPI, base_name='purchases')


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'make_order', purchase_items_from_basket)
]