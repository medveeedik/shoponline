from basket.models import BasketGoods, Basket
from django.contrib import admin


admin.site.register(BasketGoods)
admin.site.register(Basket)

