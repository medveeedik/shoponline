from django.db import models
from django.utils.translation import ugettext_lazy as _

from base.models import UUIDModel
from good.models import Good
from users.models import User


class Basket(UUIDModel):
    goods = models.ManyToManyField(Good, through='BasketGoods')
    user = models.OneToOneField(User, related_name='basket', on_delete=models.CASCADE, null=True,
                                blank=True)

    def clear(self):
        self.basketgoods_set.clear()

    def __str__(self):
        return 'Basket of ' + str(self.user)


class BasketGoods(UUIDModel):
    good = models.ForeignKey(Good, on_delete=models.CASCADE, null=True)
    basket = models.ForeignKey(Basket, on_delete=models.CASCADE, null=True)
    amount = models.DecimalField(decimal_places=2, max_digits=10, null=False, blank=False,
                                 default=0.0)


class Transfers(UUIDModel):
    DEPOSIT = 'deposit payment'
    WITHDRAW = 'withdraw payment'

    APPROVED = 'approved'
    PENDING = 'pending'
    REJECTED = 'rejected'

    PAYMENT_TYPES = (
        (DEPOSIT, DEPOSIT),
        (WITHDRAW, WITHDRAW))

    PAYMENT_STATUS = (
        (APPROVED, APPROVED),
        (PENDING, PENDING),
        (REJECTED, REJECTED))

    amount = models.DecimalField(max_digits=8, decimal_places=2, null=False, blank=False)
    payment_type = models.CharField(
        _('Payment type'), blank=False, choices=PAYMENT_TYPES, default=DEPOSIT, max_length=30)
    payment_status = models.CharField(
        _('Payment status'), blank=False, choices=PAYMENT_STATUS, default=APPROVED, max_length=30)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='transfers')


class Purchase(UUIDModel):
    goods = models.ManyToManyField(Good, through='PurchaseGoods')
    user = models.ForeignKey(User, related_name='purchases', on_delete=models.CASCADE, null=True,
                             blank=True)
    transfer = models.OneToOneField(null=True, on_delete=models.CASCADE, related_name='transfer',
                                    to='basket.Transfers')


class PurchaseGoods(UUIDModel):
    good = models.ForeignKey(Good, on_delete=models.SET_NULL, null=True)
    purchase = models.ForeignKey(Purchase, on_delete=models.SET_NULL, null=True)
    amount = models.DecimalField(decimal_places=2, max_digits=10, null=False, blank=False,
                                 default=0.0)

