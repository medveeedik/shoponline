from rest_framework import serializers

from basket.models import BasketGoods, Transfers, Purchase, PurchaseGoods
from good.serializers import GoodSerializer
from good.models import Good


class OrdersSerializer(serializers.ModelSerializer):
    good = serializers.PrimaryKeyRelatedField(
        required=True, help_text='Good id.',
        queryset=Good.objects.all())

    class Meta:
        model = BasketGoods
        fields = ('id', 'good', 'amount')


class UpdateOrderSerializer(serializers.ModelSerializer):
    amount = serializers.DecimalField(decimal_places=2, max_digits=10, required=True)
    good = GoodSerializer(read_only=True)

    class Meta:
        model = BasketGoods
        fields = ('id', 'good', 'amount')


class TransfersSerializer(serializers.ModelSerializer):
    amount = serializers.DecimalField(decimal_places=2, max_digits=10, required=True)
    payment_status = serializers.ChoiceField(
        read_only=True, choices=Transfers.PAYMENT_STATUS)

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        return super(TransfersSerializer, self).create(validated_data)

    class Meta:
        model = Transfers
        fields = ('id', 'amount', 'created', 'payment_type', 'payment_status')


class GoodToPurchaseSerializer(serializers.ModelSerializer):
    good = GoodSerializer()

    class Meta:
        model = PurchaseGoods
        fields = ('id', 'good', 'amount')


class PurchaseSerializer(serializers.ModelSerializer):
    goods = GoodToPurchaseSerializer(many=True, source='purchasegoods_set')

    class Meta:
        model = Purchase
        fields = ('id', 'goods')
