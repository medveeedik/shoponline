# Generated by Django 2.1.4 on 2018-12-07 00:22

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('good', '0007_auto_20181206_2236'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('basket', '0005_transfers_payment_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='Purchase',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created', models.DateTimeField(auto_now_add=True, null=True, verbose_name='date/time created')),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PurchaseGoods',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('created', models.DateTimeField(auto_now_add=True, null=True, verbose_name='date/time created')),
                ('modified', models.DateTimeField(auto_now=True, null=True)),
                ('amount', models.DecimalField(decimal_places=2, default=0.0, max_digits=10)),
                ('good', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='good.Good')),
                ('purchase', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='basket.Purchase')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='purchase',
            name='goods',
            field=models.ManyToManyField(through='basket.PurchaseGoods', to='good.Good'),
        ),
        migrations.AddField(
            model_name='purchase',
            name='user',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='purchases', to=settings.AUTH_USER_MODEL),
        ),
    ]
