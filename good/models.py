from django.db import models

from base.models import UUIDModel


class Category(UUIDModel):
    title = models.CharField(max_length=100, null=False, blank=False, unique=True)

    def __str__(self):
        return self.title


class Manufacturer(UUIDModel):
    name = models.CharField(max_length=100, null=False, blank=False, unique=True)
    country = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return self.name + ' from ' + self.country


class OptionCategory(UUIDModel):
    name = models.CharField(max_length=100, null=False, blank=False, unique=True)

    def __str__(self):
        return self.name


class Option(UUIDModel):
    name = models.CharField(max_length=100, null=False, blank=False, unique=True)
    category = models.ForeignKey(OptionCategory, related_name='options', null=True,
                                 on_delete=models.SET_NULL)

    def __str__(self):
        return self.name


class Good(UUIDModel):
    title = models.CharField(max_length=100, null=False, blank=False)
    description = models.TextField(null=False, blank=True)
    cost = models.DecimalField(max_digits=8, decimal_places=2, null=False, blank=False)
    background_image = models.ImageField(upload_to='static/images', max_length=500, blank=True,
                                         null=True)
    categories = models.ManyToManyField(Category, related_name='goods')
    options = models.ManyToManyField(Option, related_name='goods')
    manufacturer = models.ForeignKey(Manufacturer, related_name='goods', null=True,
                                     on_delete=models.SET_NULL)

    def __str__(self):
        return self.title + '  $ ' + str(self.cost)

