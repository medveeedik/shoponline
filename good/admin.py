from good.models import Category, Good, Manufacturer, OptionCategory, Option
from django.contrib import admin


admin.site.register(Category)
admin.site.register(Good)
admin.site.register(Manufacturer)
admin.site.register(OptionCategory)
admin.site.register(Option)
