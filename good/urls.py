from django.conf.urls import url, include

from base.routers import CustomRouter
from good.api import CategoryAPI, GoodsAPI, OptionAPI, OptionCategoryAPI, ManufacturerAPI


router = CustomRouter(trailing_slash=True)
router.register(r'categories', CategoryAPI, base_name='categories')
router.register(r'goods', GoodsAPI, base_name='goods')
router.register(r'manufacturer', ManufacturerAPI, base_name='manufacturer')
router.register(r'options', OptionAPI, base_name='options')
router.register(r'category-options', OptionCategoryAPI, base_name='option-categories')


urlpatterns = [
    url(r'^', include(router.urls)),
]