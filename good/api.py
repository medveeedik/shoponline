from rest_framework import mixins, viewsets
from good.models import Category, Good, Manufacturer, OptionCategory, Option
from good.serializers import CategorySerializer, GoodSerializer, ManufacturerSerializer, \
    OptionCategorySerializer, OptionSerializer


class CategoryAPI(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):

    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class OptionCategoryAPI(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):

    queryset = OptionCategory.objects.all()
    serializer_class = OptionCategorySerializer


class OptionAPI(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):

    queryset = Option.objects.all()
    serializer_class = OptionSerializer


class ManufacturerAPI(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):

    queryset = Manufacturer.objects.all()
    serializer_class = ManufacturerSerializer


class GoodsAPI(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):

    queryset = Good.objects.all()
    serializer_class = GoodSerializer

