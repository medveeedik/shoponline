from rest_framework import serializers

from good.models import Category, Good, Manufacturer, OptionCategory, Option


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'title')


class OptionCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = OptionCategory
        fields = ('id', 'name')


class OptionSerializer(serializers.ModelSerializer):
    category = OptionCategorySerializer(read_only=True)

    class Meta:
        model = Option
        fields = ('id', 'name', 'category')


class ManufacturerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Manufacturer
        fields = ('id', 'name', 'country')


class GoodSerializer(serializers.ModelSerializer):
    categories = CategorySerializer(read_only=True, many=True)
    options = OptionSerializer(read_only=True, many=True)
    manufacturer = ManufacturerSerializer(read_only=True,)

    class Meta:
        model = Good
        fields = ('id', 'title', 'description', 'cost', 'background_image', 'categories',
                  'manufacturer', 'options')
